
local Graphics = love.graphics
local Filesystem = love.filesystem
local Floor = math.floor
UI = {
	
	searchForFile = function(root,file)
		for _,item in ipairs(Filesystem.getDirectoryItems(root)) do
			if Filesystem.isDirectory(root.."/"..item) and item ~= file then
				local path = UI.searchForFile(root.."/"..item,file)
				if path then
					return path
				end
			elseif item == file then
				return root.."/"..item
			end
		end
		return false
	end,
	setCursor = function(file,x,y)
		local x,y = tostring(x),tostring(y)
		if UI.Cursors[file] then
			local temp = UI.Cursors[file]
			if temp.Poses[x] then
				if temp.Poses[x][y] then
					local cur = temp.Poses[x][y]
					love.mouse.setCursor(cur)
					return
				end
			end
		end
		local path = UI.searchForFile(UI.Source,file)
		if not path then
			error("Could not find image file: "..file)
		end
		local tab = false
		if not UI.Cursors[file] then
			UI.Cursors[file] = {}
		end
		if not UI.Cursors[file]["Image"] then
			UI.Cursors[file]["Image"] = love.image.newImageData(path)
		end
		if UI.Cursors[file] then
			tab = UI.Cursors[file]
			if not tab.Poses then
				tab.Poses = {}
			end
			if not tab.Poses[x] then
				tab.Poses[x] = {}
			end
			if not tab.Poses[x][y] then
				tab.Poses[x][y] = love.mouse.newCursor( tab.Image, tonumber(x), tonumber(y) )
			end
		else
			local poses = {}
			poses[x] = {}
			poses[x][y] = love.mouse.newCursor( tab.Image, tonumber(x), tonumber(y) )
			tab = {
				Poses = poses,
				Image = love.image.newImageData(path)
			}
		end
		UI.Cursors[file] = tab
		love.mouse.setCursor(UI.Cursors[file]["Poses"][x][y])
	end,
	Cursors = {},
	initImages = function()
		UI.StoredImages = {}
	end,
	StoredImages = {},
	getImage = function(file)
		if UI.StoredImages[file] then
			return UI.StoredImages[file]
		end
		local path = UI.searchForFile(UI.Source,file)
		if path then
			UI.newImage(path,file)
			return UI.getImage(file)
		else
			error("Could not find image file: "..file)
		end
	end,
	Source = "",
	setSource = function(sr)
		UI.Source = sr
	end,
	newImage = function(fileName,tag)
		local u = {}
		local Image = Graphics.newImage(fileName)
		u.Image = Image
		local PicWidth, PicHeight = Image:getDimensions()
		u.Width = PicWidth
		u.Height = PicHeight
		UI.StoredImages[tag] = u
	end,
	Active = true,
	activate = function(bool)
		UI.Active = bool
	end,
	MouseX = 0,
	MouseY = 0,
	MouseIsDown = false,
	mouseReleased = false,
	AnimationSettings = {
		hover = 0.2,
		pressed = -0.2,
	},
	playSound = function(file,stop,loop)
		if UI.Sounds[file] then
			local temp = UI.Sounds[file]
			if stop then
				love.audio.stop(temp)
			end
			temp:setLooping(loop)
			love.audio.play(temp)
			return
		end
		
		local path = UI.searchForFile(UI.Source,file)
		if path then
			local size = Filesystem.getSize(path)
			if size < 1000000 then
				UI.Sounds[file] = love.audio.newSource(path,"static")
			else
				UI.Sounds[file] = love.audio.newSource(path,"stream")
			end
			local temp = UI.Sounds[file]
			temp:setLooping(loop)
			love.audio.play(temp)
		else
			error("Could not find audio file: "..file)
		end
		
	end,
	Sounds = {},
	initSounds = function()
		UI.Sounds = {}
	end,
	setShader = function(file)
		if not file then
			Graphics.setShader()
			return
		end
		if UI.Shaders[file] then
			local temp = UI.Shaders[file]
			Graphics.setShader(temp)
			return
		end
		
		local path = UI.searchForFile(UI.Source,file)
		if path then
			local ch = Filesystem.load(path)
			UI.Shaders[file] = Graphics.newShader(ch())
			local temp = UI.Shaders[file]
			Graphics.setShader(temp)
		else
			error("Could not find glsl file: "..file)
		end
		
	end,
	getShader = function(file)
		if UI.Shaders[file] then
			return UI.Shaders[file]
		end
	end,
	Shaders = {},
	initShaders = function()
		UI.Shaders = {}
	end,
	mouseButton = 1,
	stackSound = function(file,layers,loop)
		if UI.SoundStack[file] then
			local tab = UI.SoundStack[file]
			tab.Current = (tab.Current+1)%layers
			if tab.Sounds[tab.Current+1] then
				local temp = tab.Sounds[tab.Current+1]
				temp:setLooping(loop)
				love.audio.stop(temp)
				love.audio.play(temp)
				return
			end
		end
		local path = UI.searchForFile(UI.Source,file)
		if path then
			local size = Filesystem.getSize(path)
			local tab = false
			if UI.SoundStack[file] then
				tab = UI.SoundStack[file]
			else
				UI.SoundStack[file] = {
					Current = 0,
					Sounds = {}
				}
				tab = UI.SoundStack[file]
			end
			
			if size*layers < 2000000 then
				UI.SoundStack[file]["Sounds"][tab.Current+1] = love.audio.newSource(path,"static")
			else
				UI.SoundStack[file]["Sounds"][tab.Current+1] = love.audio.newSource(path,"stream")
			end
		else
			error("Could not find audio file: "..file)
		end
		local tab = UI.SoundStack[file]
		local temp = tab.Sounds[tab.Current+1]
		temp:setLooping(loop)
		love.audio.play(temp)
	end,
	SoundStack = {},
	initStackSound = function()
		UI.SoundStack = {}
	end,
	setFont = function(file,size)
		if UI.Fonts[file] then
			if UI.Fonts[file][size] then
				Graphics.setFont(UI.Fonts[file][size])
				return
			end
		end
		local path = UI.searchForFile(UI.Source,file)
		if not path then
			error("Could not find font file: "..file)
		end
		if not UI.Fonts[file] then
			UI.Fonts[file] = {}
		end
		if not UI.Fonts[file][size] then
			UI.Fonts[file][size] = Graphics.newFont( path, tonumber(size) )
		end
		Graphics.setFont(UI.Fonts[file][size])
	end,
	text = function(str,x,y,wrap,color,font,size)
	if font then
		UI.setFont(font,size)
	end
	if color then
		Graphics.setColor(color)
	end
		Graphics.printf(str,Floor(x),Floor(y),wrap)
	end,
	Fonts = {},
	element = function(ui,BX,BY,BW,BH,Animate,Sound)
		if ui then
			ui = UI.getImage(ui)
		end
		--BX = Floor(BX)
		--BY = Floor(BY)
		if UI.MouseX > BX and UI.MouseX < (BX+BW) and UI.MouseY > BY and UI.MouseY < (BY+BH) and UI.Active then
			if UI.MouseIsDown then
				if ui then
					if Animate then
						Graphics.draw(ui.Image,BX+(BW*(-UI.AnimationSettings.pressed*.5)),BY+(BH*(-UI.AnimationSettings.pressed*.5)),0,(1+UI.AnimationSettings.pressed)*((BW/ui.Width)),(1+UI.AnimationSettings.pressed)*((BH/ui.Height)))
					else
						Graphics.draw(ui.Image,BX,BY,0,((BW/ui.Width)),((BH/ui.Height)))
					end
				end
				return "pressed"..tostring(UI.mouseButton)
			elseif not UI.MouseIsDown then
				if ui then
					if Animate then
						Graphics.draw(ui.Image,BX+(BW*(-UI.AnimationSettings.hover*.5)),BY+(BH*(-UI.AnimationSettings.hover*.5)),0,(1+UI.AnimationSettings.hover)*((BW/ui.Width)),(1+UI.AnimationSettings.hover)*((BH/ui.Height)))
					else
						Graphics.draw(ui.Image,BX,BY,0,((BW/ui.Width)),((BH/ui.Height)))
					end
				end
				
			end
			if UI.mouseReleased then
				if Sound then
					UI.playSound(Sound,true)
				end
				return "click"..tostring(UI.mouseButton)
			end
			return "hover"
		else
		if ui then
			Graphics.draw(ui.Image,BX,BY,0,((BW/ui.Width)),((BH/ui.Height)))
		end		
		end
		return false
	end,
	concatKeyToString = function(str,limit)
		local limit = limit
		if not limit then
			limit = 999
		end
		if UI.KeyIsDown == "backspace" then
			str = string.sub(str,1,#str-1)
		elseif UI.textInput and #str < limit then
			str = str..UI.textInput
		end
		return str
	end,
	CanvasTab = {},
	CanvasObj = function(w,h)
		return {
			Canvas = Graphics.newCanvas( w,h,"normal",0 ),
			Width = w,
			Height = h,
		}
	end,
	toggleCanvas = function(CAN,w,h)
		if not CAN then
			Graphics.setCanvas(Game.Canvas)
			return
		end
		if UI.CanvasTab[CAN] then
			local nc = UI.CanvasTab[CAN]
			if nc.Width ~= w or nc.Height ~= h then
				nc = UI.CanvasObj(w,h)
				UI.CanvasTab[CAN] = nc
			end
			Graphics.setCanvas(nc.Canvas)
		else
			UI.CanvasTab[CAN] = UI.CanvasObj(w,h)
			Graphics.setCanvas(UI.CanvasTab[CAN]["Canvas"])
		end
	end,
	drawCanvas = function(CAN,x,y)
		if UI.CanvasTab[CAN] then
			local c = UI.CanvasTab[CAN]
			Graphics.draw(c.Canvas,x,y)
		end
	end,
	dataFromCanvas = function(CAN)
		if UI.CanvasTab[CAN] then
			return UI.CanvasTab[CAN]["Canvas"]:newImageData()
		else
			return false
		end
	end,
	KeyIsDown = false,
	textInput = false,
	WindowWidth = 100,
	WindowHeight = 100,
	rescaleX = 1,
	rescaleY = 1,
	UPTime = 0,
	Width = 800,
	Height = 600,
	X = 0,
	Y = 0,
	Fullscreen = love.window.getFullscreen( ),
	
}


Game = {
	GameStateCode = {},
	Source = "",
	setSource = function(sr)
		Game.Source = sr
	end,
	Force = false,
	NewState = false,
	setState = function(file,force)
		Game.NewState = file
		Game.Force = force == true
		if not Game.GameStateCode[file] then
			local path = UI.searchForFile(Game.Source,file)
			if not path then
				error("Could not find lua file: "..file)
			end
			local ch = Filesystem.load(path)
			Game.GameStateCode[file] = ch()
		end
	end,
	setup = function(params)
		if not params["State"] then
			params["State"] = "Splash.lua"
		end
		if params["Width"] then
			UI.Width = params["Width"]
		end
		if params["Height"] then
			UI.Height = params["Height"]
		end
		if params["Source"] then
			Game.Source = params["Source"]
		end
		Game.setState(params["State"])
		Game.Canvas = Graphics.newCanvas( UI.Width,UI.Height,"normal",0 )
		love.resize(Graphics.getDimensions())
		Game.CurrentState = Game.GameStateCode[Game.NewState]
		Game.CurrentState.loadup(Game.CurrentState)
		Game.NewState = false
	end,
	Canvas = false,
}


Entity = {
	Source = "",
	Scripts = {},
	Objects = {},
	loadNewScript = function(file)
		local path = UI.searchForFile(Entity.Source,file)
		if not path then
			error("Could not find lua file: "..file)
		end
		local ch = Filesystem.load(path)
		Entity.Scripts[file] = ch()
		if not Entity.Objects[file] then
			Entity.Objects[file] = {}
		end
	end,
	new = function(file,args)
		if not Entity.Scripts[file] then
			Entity.loadNewScript(file)
		end
		local script = Entity.Scripts[file]
		if not Entity.Objects[file] then
			Entity.Objects[file] = {}
		end
		table.insert(Entity.Objects[file],script.new(args))
	end,
	delete = function(file,e)
		table.remove(Entity.Objects[file],e)
	end,
	deleteAll = function(file)
		Entity.Objects[file] = {}
	end,
	update = function(file,dt)
		if not Entity.Objects[file] then
			return false
		end
		local script = Entity.Scripts[file]
		for e,entity in ipairs(Entity.Objects[file]) do
			script.update(entity,e,dt)
		end
	end,
	draw = function(file)
		if not Entity.Objects[file] then
			return false
		end
		local script = Entity.Scripts[file]
		for e,entity in ipairs(Entity.Objects[file]) do
			script.draw(entity,e)
		end
	end,
	add = function(file,new)
		if not Entity.Objects[file] then
			Entity.Objects[file] = {}
		end
		if not Entity.Scripts[file] then
			Entity.loadNewScript(file)
		end
		for n,data in ipairs(new) do
			table.insert(Entity.Objects[file],data)
		end
	end,
	get = function(file)
		if Entity.Objects[file] then
			return Entity.Objects[file]
		end
	end,
	swap = function(file,new)
		local new = new
		if Entity.Objects[file] then
			Entity.Objects[file] = new
		end
	end,
}

dscript = {
	Source = "",
	Scripts = {},
	loadNewScript = function(file)
		local path = UI.searchForFile(Entity.Source,file)
		if not path then
			error("Could not find lua file: "..file)
		end
		local ch = Filesystem.load(path)
		Entity.Scripts[file] = ch()
	end,
	f = function(file,fun,args,key)
		if not Entity.Scripts[file] then
			Entity.loadNewScript(file)
		end
		local script = Entity.Scripts[file][fun]
		return script(args,key)
	end,
}



Splash = {
	imageToObject = function(name)
		local obj = {}
		local Pic = Graphics.newImage(name)
		obj.Pic = Pic
		local PicWidth, PicHeight = Pic:getDimensions()
		obj.Width = PicWidth
		obj.Height = PicHeight
		obj.Name = name
		return obj
	end,
	init = function()
		Splash.SplashScreens = {}
	end,
	SplashScreens = {},
	add = function(name)
		table.insert(Splash.SplashScreens,Splash.imageToObject("Assets/Splash/"..name))
	end,
	reset = function(fadeIn,stay,fadeOut)
		Splash.CurrentSplash = 1
		for s,sp in ipairs(Splash.SplashScreens) do
			sp.stayTimer = stay
			sp.fadeInTimer = fadeIn
			sp.fadeOutTimer = fadeOut
			sp.fadeInTimerOG = fadeIn
			sp.fadeOutTimerOG = fadeOut
			sp.State = "fadeIn"
		end
	end,
	CurrentSplash = 1,
	update = function(dt)
		if Splash.CurrentSplash > #Splash.SplashScreens then
			return false
		end
		for s,sp in ipairs(Splash.SplashScreens) do
		
			if s == Splash.CurrentSplash then
				if sp.State == "fadeIn" then
					sp.Alpha = 250-((sp.fadeInTimer/sp.fadeInTimerOG)*250)
					sp.fadeInTimer = sp.fadeInTimer-dt
					
					if sp.fadeInTimer < 0 then
						sp.State = "Stay"
					end
				elseif sp.State == "Stay" then
					sp.Alpha = 255
					sp.stayTimer = sp.stayTimer-dt
					if sp.stayTimer < 0 then
						sp.State = "fadeOut"
					end
				elseif sp.State == "fadeOut" then
					sp.Alpha = (sp.fadeOutTimer/sp.fadeOutTimerOG)*250
					sp.fadeOutTimer = sp.fadeOutTimer-dt
					if sp.fadeOutTimer < 0 then
						sp.State = "next"
					end
				elseif sp.State == "next" then
					Splash.CurrentSplash = Splash.CurrentSplash+1
				end
			end
		end
		return true
	end,


	draw = function()
		for s,sp in ipairs(Splash.SplashScreens) do
			if s == Splash.CurrentSplash then
				Graphics.clear()
				Graphics.setColor(255,255,255,sp.Alpha)
				Graphics.draw(sp.Pic,0,0,0,UI.Width/sp.Width,UI.Height/sp.Height)
			end
		end

	end
}


function love.resize(x,y)

	UI.WindowWidth = x
	UI.WindowHeight = y
	UI.rescaleX = UI.WindowWidth/UI.Width--
	UI.rescaleY = UI.WindowHeight/UI.Height--

end
love.resize(Graphics.getDimensions())
Graphics.setDefaultFilter('nearest', 'nearest')
function love.keypressed(key)
	UI.KeyIsDown = key
end
function love.textinput(t)
    UI.textInput = t
end

function love.touchpressed( id, x, y, dx, dy, pressure )
	UI.MouseX = x*UI.WindowWidth
	UI.MouseY = y*UI.WindowHeight
end
--
function love.touchmoved( id, x, y, dx, dy, pressure )
	UI.MouseX = x*UI.WindowWidth
	UI.MouseY = y*UI.WindowHeight
	UI.MouseIsDown = true
end
--
function love.touchreleased( id, x, y, dx, dy, pressure )
	UI.MouseX = x*UI.WindowWidth
	UI.MouseY = y*UI.WindowHeight
	UI.mouseReleased = true
	UI.MouseIsDown = false
	UI.mouseButton = 1
end
--
function love.mousereleased(x, y, button)
	UI.mouseReleased = true
	UI.MouseIsDown = false
	UI.mouseButton = button
end
function love.mousepressed(x, y, button)
	UI.mouseButton = button
end
local altEPressed = false

Game.update = function(dt)
	Graphics.setCanvas(Game.Canvas)
	if love.keyboard.isDown("ralt") and love.keyboard.isDown("return") and not altEPressed then
		altEPressed = true
		UI.Fullscreen = UI.Fullscreen == false
		love.window.setFullscreen(UI.Fullscreen,"desktop")
	elseif not love.keyboard.isDown("return") then
		altEPressed = false
	end
	UI.UPTime = UI.UPTime+dt
	UI.MouseIsDown = love.mouse.isDown(1,2,3)
	UI.MouseX, UI.MouseY = love.mouse.getPosition()
	UI.MouseX = UI.MouseX/UI.rescaleX
	UI.MouseY = UI.MouseY/UI.rescaleY
	Game.CurrentState.update(Game.CurrentState,dt)
end
Game.draw = function()
	
	if Game.Force then
		Game.Force = false
		Game.CurrentState.draw(Game.CurrentState)
		Game.CurrentState = Game.GameStateCode[Game.NewState]
		Game.CurrentState.loadup(Game.CurrentState)
	else
		Game.CurrentState.draw(Game.CurrentState)
		if Game.NewState then
			Game.CurrentState = Game.GameStateCode[Game.NewState]
			Game.CurrentState.loadup(Game.CurrentState)
			Game.NewState = false
		end
	end
	Graphics.setColor(255,255,255,255)
	Graphics.setCanvas()
	Graphics.draw(Game.Canvas,0,0,0,UI.rescaleX,UI.rescaleY)
	UI.mouseReleased = false
	UI.KeyIsDown = false
	UI.textInput = false
end