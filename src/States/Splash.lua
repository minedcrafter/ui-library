return {
    
	loadup = function(self)
		Splash.init()
		Splash.add("Your splash image.png")
		Splash.add("Your splash image2.png") -- add as many as you want
		Splash.reset(1,2,1) -- fade in, hold, fade out
	end,
	
	update = function(self,dt)
		if not Splash.update(dt) then
			Game.setState("empty.lua") -- will stop running this code and switch to a different state
		end
	end,

	draw = function(self)
		Splash.draw()
	end,

}